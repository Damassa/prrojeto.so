Trabalho de Sistemas Operacionais
Integrantes:
Bruno Ribeiro RA:21908124
Gabriel Damaceno RA:21951337
Heitor Medeiros RA:21904773

Execução:
Abra o Terminal.
Linux:

- Usar o comando mkdir para criar uma pasta.
  exemplo: mkdir pasta
- Usar o comando cd para entrar na pasta criada, Exemplo: cd pasta.
- Executar no terminal: git clone https://gitlab.com/Damassa/Prrojeto.so.git
- Executar comando: cd Sistemas-Operacionais.(nome do diretório).
- Executar comando: python Threads.py para executar o script.
- 
Windows:

- Executar o comando no terminal: git clone https://gitlab.com/Damassa/Prrojeto.so.git
- Encontrar o Executável do python em sua máquina.
- Depois, encontrar o script .py presente na pasta Prrojeto.so.
- Colocar o endereço do python no terminal entre aspas e depois um espaço e o endereço do arquivo .py.
Exemplo: "C:\Program Files (x86)\Microsoft Visual Studio\Shared\Python36_64\python.exe" C:\Users\Bruno\Desktop\prrojeto.so\projetoso.py
- Depois, deve clicar na tecla ENTER, e o script irá rodar.